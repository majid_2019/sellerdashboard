/**
 * 
 */
package com.tph.handler;

import com.google.gson.Gson;
import com.tph.dao.DashboardDataDao;
import com.tph.request.SellerBaseRequestModel;
import com.tph.response.SellerBaseResponseModel;

/**
 * @author majidkhan
 *
 */
public class TestSubCategoryId {
	public static void main(String[] args) {
		SellerBaseResponseModel sellerBaseResponseModel = new SellerBaseResponseModel();
		
		SellerBaseRequestModel dashboardRequestModel = new SellerBaseRequestModel();
		dashboardRequestModel.setIndustrialTypeId(2);;

		sellerBaseResponseModel = new DashboardDataDao().fetchCategoryMaster(dashboardRequestModel, sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));

	}
}
