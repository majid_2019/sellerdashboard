/**
 * 
 */
package com.tph.handler;

import com.google.gson.Gson;
import com.tph.dao.DashboardDataDao;
import com.tph.request.EditProfilleRequest;
import com.tph.response.SellerBaseResponseModel;

/**
 * @author majidkhan
 *
 */
public class TestUpdateProfile {
	public static void main(String[] args) {
		SellerBaseResponseModel sellerBaseResponseModel = new SellerBaseResponseModel();
		//ProfileViewResponseModel sellerAddNewPostRequestModel = new ProfileViewResponseModel();
		EditProfilleRequest editProfilleRequest = new EditProfilleRequest();
		
		editProfilleRequest.setUserId(1);
		editProfilleRequest.setFirstName("Majid");
		editProfilleRequest.setLastName("Khan");
		editProfilleRequest.setEmailId("mazidkhan008@gmail.com");
		editProfilleRequest.setMobileNumber(8109536446L);
		
		
		sellerBaseResponseModel = new DashboardDataDao().updateProfileData(editProfilleRequest, sellerBaseResponseModel);
		
		System.out.println("Request - "+new Gson().toJson(sellerBaseResponseModel));

	}

}
