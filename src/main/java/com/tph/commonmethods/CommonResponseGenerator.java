/**
 * 
 */
package com.tph.commonmethods;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jooq.Record;
import org.jooq.Result;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.tph.constants.ConstantKeys;
import com.tph.jooq.tph_db.tables.CityMaster;
import com.tph.jooq.tph_db.tables.CountryMaster;
import com.tph.jooq.tph_db.tables.StateMaster;
import com.tph.jooq.tph_db.tables.Users;
import com.tph.jooq.tph_db.tables.records.IndustrialTypeRecord;
import com.tph.response.IndusDataModel;
import com.tph.response.IndustrialCateData;
import com.tph.response.IndustrialSubCateData;
import com.tph.response.ProfileViewResponseModel;
import com.tph.response.SellerBaseResponseModel;

/**
 * @author majidkhan
 *
 */
public class CommonResponseGenerator {

	/**
	 * @param record
	 * @return
	 * @throws Exception
	 */
	public ProfileViewResponseModel getProfileData(Record record) throws Exception {
		ProfileViewResponseModel profileViewResponseModel = new ProfileViewResponseModel();
		if (record != null) {
			System.out.println(record.get(Users.USERS.ID));
			System.out.println(record.get(StateMaster.STATE_MASTER.STATE));

			profileViewResponseModel.setCompanyName(record.get(Users.USERS.COMPANY));
			profileViewResponseModel
					.setContactPerson(record.get(Users.USERS.FIRST_NAME) + " " + record.get(Users.USERS.LAST_NAME));
			profileViewResponseModel.setMobileNumber(record.get(Users.USERS.USER_MOBILE));
			profileViewResponseModel.setEmailId(record.get(Users.USERS.EMAIL));
			profileViewResponseModel.setAddress(record.get(Users.USERS.ADDRESS_1) + " "
					+ record.get(Users.USERS.ADDRESS_2) + " " + record.get(Users.USERS.ADDRESS_3));
			profileViewResponseModel.setCountryName(record.get(CountryMaster.COUNTRY_MASTER.NAME));
			profileViewResponseModel.setStateName(record.get(StateMaster.STATE_MASTER.STATE));
			profileViewResponseModel.setCityName(record.get(CityMaster.CITY_MASTER.CITY));
			profileViewResponseModel.setGstNumber(record.get(Users.USERS.GST_NO));
		}
		return profileViewResponseModel;
	}

	/**
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public SellerBaseResponseModel getSuccessMsgResponse(SellerBaseResponseModel sellerBaseResponseModel) {
		sellerBaseResponseModel.setStatus(ConstantKeys.SUCCESS_CODE);
		sellerBaseResponseModel.setMessage(ConstantKeys.SUCCESS_MSG);

		return sellerBaseResponseModel;
	}

	/**
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public SellerBaseResponseModel getBasReqMsgResponse(SellerBaseResponseModel sellerBaseResponseModel) {
		sellerBaseResponseModel.setStatus(ConstantKeys.BAD_REQUEST_CODE);
		sellerBaseResponseModel.setMessage(ConstantKeys.BAD_REQUEST_MSG);

		return sellerBaseResponseModel;
	}

	/**
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public SellerBaseResponseModel getExceptionResponse(SellerBaseResponseModel sellerBaseResponseModel) {
		sellerBaseResponseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
		sellerBaseResponseModel.setMessage(ConstantKeys.INTERNAL_SERVER_ERROR_MSG);
		return sellerBaseResponseModel;
	}

	/**
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public SellerBaseResponseModel getLambdaFailResponse(SellerBaseResponseModel sellerBaseResponseModel) {
		sellerBaseResponseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
		sellerBaseResponseModel.setMessage(ConstantKeys.FAIL);
		return sellerBaseResponseModel;
	}

	/**
	 * @param result
	 */
	public void fetchIndusrialList(Result<Record> result) {

		IndusDataModel indusDataModel = null;
		IndustrialCateData industrialCateData;
		JSONArray indType = new JSONArray();
		JSONArray indCatTypeArray = new JSONArray();

		Map<Integer, String> mapIndustrial = new HashMap<Integer, String>();
		Map<Integer, String> mapIndusCategory = new HashMap<Integer, String>();

		JSONObject jsonObjCatMaster;
		JSONObject jsonObjIndType = null;
		List<String> catMster = new ArrayList<String>();

		List<String> subCatMster = new ArrayList<String>();
		List<IndustrialCateData> catList = new ArrayList<>();
		// System.out.println("Result-- "+result);

		System.out.println(result);

		for (int i = 0; i < result.size(); i++) {
			jsonObjIndType = new JSONObject();
			jsonObjCatMaster = new JSONObject();

			if (mapIndustrial.size() > 0)
				for (Map.Entry<Integer, String> entry : mapIndustrial.entrySet()) {

					// System.out.println("("+i+")"+" Key --- "+entry.getKey() +"
					// ====="+result.get(i).getValue("ind_id") + " --- equal ---
					// "+entry.getKey().equals((Integer) result.get(i).getValue("ind_id")));

					if (!entry.getKey().equals((Integer) result.get(i).getValue("ind_id"))) {
						industrialCateData = new IndustrialCateData();
						indusDataModel = new IndusDataModel();

						// Store Industries in Object
						indusDataModel.setId((Integer) result.get(i).getValue("ind_id"));
						indusDataModel.setIndusName((String) result.get(i).getValue("industrial_name"));
						indType.put(indusDataModel);

						// Store Industries Categories in Object
						industrialCateData.setId((Integer) result.get(i).getValue("cat_master_id"));
						industrialCateData.setIndusCatName((String) result.get(i).getValue("industrial_cat_name"));
						catList.add(industrialCateData);

						indusDataModel.setIndustrialCateData(catList);

						/*
						 * indType.put(indusDataModel); indusDataModel.setIndustrialCateData(catList);
						 * indCatTypeArray.put(indusDataModel);
						 */

					}
				}

			mapIndustrial.put((Integer) result.get(i).getValue("ind_id"),
					(String) result.get(i).getValue("industrial_name"));
		}
		System.out.println(new Gson().toJson(indType));
	}

	/**
	 * Store all Industrial data in List
	 * 
	 * @param result
	 * @return
	 * @throws Exception
	 */
	public List<IndusDataModel> IndustrialTypeRecord(Result<IndustrialTypeRecord> result) throws Exception {
		IndusDataModel indusDataModel = null;
		List<IndusDataModel> list = new ArrayList<>();

		if (result.size() > 0) {
			for (int i = 0; i < result.size(); i++) {
				indusDataModel = new IndusDataModel();

				indusDataModel.setId(result.get(i).getId());
				indusDataModel.setIndusName(result.get(i).getName().replace("\u0026", "&"));

				list.add(indusDataModel);
			}
		}

		return list;

	}

	/**
	 * Create Json for Industrial category data.
	 * 
	 * @param fetchCategoryMaster
	 * @return 
	 */
	public JSONObject fetchIndCategoryList(Result<Record> fetchCategoryMaster) {
		IndustrialCateData industrialCateData;
		Set<IndustrialCateData> set = new HashSet<IndustrialCateData>();
		if (fetchCategoryMaster.size() > 0) {
			for (int i = 0; i < fetchCategoryMaster.size(); i++) {
				industrialCateData = new IndustrialCateData();
				industrialCateData.setId((Integer) fetchCategoryMaster.get(i).getValue("cat_master_id"));
				industrialCateData.setIndusCatName((String) fetchCategoryMaster.get(i).getValue("cat_name"));

				set.add(industrialCateData);
			}
		}
		return new JSONObject().put("data", getSubCatData(set, fetchCategoryMaster));
	}

	// Using for fetch inner json of Category master..
	private List<IndustrialCateData> getSubCatData(Set<IndustrialCateData> set, Result<Record> fetchCategoryMaster) {
		System.out.println(fetchCategoryMaster);
		List<IndustrialCateData> catList = new ArrayList<>();
		IndustrialSubCateData industrialSubCateData = null;
		List<IndustrialSubCateData> subCatList = null;
		IndustrialCateData iterateCateData;

		if (set.size() > 0) {
			Iterator<IndustrialCateData> itr = set.iterator();
			while (itr.hasNext()) {
				iterateCateData = itr.next();
				subCatList = new ArrayList<>();

				for (int i = 0; i < fetchCategoryMaster.size(); i++) {
					if (iterateCateData.getId() == (Integer) fetchCategoryMaster.get(i).getValue("cat_master_id")) {
						industrialSubCateData = new IndustrialSubCateData();

						industrialSubCateData.setId((Integer) fetchCategoryMaster.get(i).getValue("sub_category_id"));
						industrialSubCateData
								.setIndusSubCatName((String) fetchCategoryMaster.get(i).getValue("sub_category_name"));
						subCatList.add(industrialSubCateData);
					}

					iterateCateData.setSubCategoryList(subCatList);
				}
				catList.add(iterateCateData);
			}
		}
		return catList;
	}
}
