/**
 * 
 */
package com.tph.commonmethods;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.jooq.Result;

import com.tph.jooq.tph_db.tables.records.CityMasterRecord;
import com.tph.jooq.tph_db.tables.records.CountryMasterRecord;
import com.tph.jooq.tph_db.tables.records.StateMasterRecord;
import com.tph.jooq.tph_db.tables.records.UomMasterRecord;
import com.tph.response.CityResponseModel;
import com.tph.response.CountryResponse;
import com.tph.response.StateResponseModel;
import com.tph.response.UomResponseModel;

/**
 * @author majidkhan
 *
 */
public class CommonMoethods {

	/**
	 * @param stateList
	 * @return
	 */
	public List<StateResponseModel> getStateList(Result<StateMasterRecord> stateList) throws Exception {
		List<StateResponseModel> list = new ArrayList<>();
		StateResponseModel stateResponseModel;

		if (stateList.size() > 0) {
			for (int i = 0; i < stateList.size(); i++) {
				stateResponseModel = new StateResponseModel();

				stateResponseModel.setStateId(stateList.get(i).getId());
				stateResponseModel.setStateName(stateList.get(i).getState());
				list.add(stateResponseModel);
			}
		}

		return list;
	}

	/**
	 * @param countryList
	 * @return
	 */
	public List<CountryResponse> getCountryList(Result<CountryMasterRecord> countryList) {
		List<CountryResponse> list = new ArrayList<>();
		CountryResponse countryResponse;

		if (countryList.size() > 0) {
			for (int i = 0; i < countryList.size(); i++) {
				countryResponse = new CountryResponse();

				countryResponse.setCountryId(countryList.get(i).getId());
				countryResponse.setCountryName(countryList.get(i).getName());
				list.add(countryResponse);
			}
		}

		return list;
	}

	/**
	 * @param stateList
	 * @return
	 */
	public List<CityResponseModel> getCityList(Result<CityMasterRecord> cityList) throws Exception {
		List<CityResponseModel> list = new ArrayList<>();
		CityResponseModel cityResponseModel;

		if (cityList.size() > 0) {
			for (int i = 0; i < cityList.size(); i++) {
				cityResponseModel = new CityResponseModel();

				cityResponseModel.setCityId(cityList.get(i).getId());
				cityResponseModel.setCityName(cityList.get(i).getCity());
				list.add(cityResponseModel);
			}
		}

		return list;
	}

	/**
	 * @param result
	 * @return
	 */
	public List<UomResponseModel> getUonList(Result<UomMasterRecord> uomList) throws Exception {
		List<UomResponseModel> uomModelList = new ArrayList<>();
		UomResponseModel uomResponseModel;

		if (uomList.size() > 0) {
			for (int i = 0; i < uomList.size(); i++) {
				uomResponseModel = new UomResponseModel();

				uomResponseModel.setUomId(uomList.get(i).getId());
				uomResponseModel.setUomName(uomList.get(i).getUomName());
				uomResponseModel.setUomAcronym(uomList.get(i).getAcronym());
				uomModelList.add(uomResponseModel);
			}
		}

		return uomModelList;
	}

	/**
	 * @param createdDate
	 * @return
	 * @throws Exception
	 */
	public String convertTimeStampToDate(Timestamp createdDate) throws Exception {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d MMMM yyyy");

		return simpleDateFormat.format(createdDate);
	}
}
