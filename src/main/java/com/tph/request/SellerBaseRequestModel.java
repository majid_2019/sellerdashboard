/**
 * 
 */
package com.tph.request;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class SellerBaseRequestModel {
	private Integer userId;
	private Integer stateId;
	private Integer cityId;
	private Integer uomId;
	private Integer countryId;
	private Integer industrialTypeId;
}
