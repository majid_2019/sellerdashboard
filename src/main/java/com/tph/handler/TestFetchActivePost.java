/**
 * 
 */
package com.tph.handler;

import com.google.gson.Gson;
import com.tph.dao.DashboardDataDao;
import com.tph.request.SellerDashboardRequestModel;
import com.tph.response.SellerBaseResponseModel;

/**
 * @author majidkhan
 *
 */
public class TestFetchActivePost {
	public static void main(String[] args) {
		
		SellerBaseResponseModel sellerBaseResponseModel = new SellerBaseResponseModel();

		SellerDashboardRequestModel dashboardRequestModel = new SellerDashboardRequestModel();
		dashboardRequestModel.setUserId(11);

		sellerBaseResponseModel = new DashboardDataDao().fetchActivePost(dashboardRequestModel, sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));
	}
}
