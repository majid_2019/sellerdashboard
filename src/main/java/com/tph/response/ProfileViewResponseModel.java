/**
 * 
 */
package com.tph.response;

import com.tph.request.SellerBaseRequestModel;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class ProfileViewResponseModel extends SellerBaseRequestModel {
	private String companyName;
	private String contactPerson;
	private Long mobileNumber;
	private String emailId;
	private String address;
	private String gstNumber;
	private String countryName;
	private String stateName;
	private String cityName;
}
