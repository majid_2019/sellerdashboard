/**
 * 
 */
package com.tph.response;

import lombok.Data;

/**
 * @author majidkhan
 */
@Data
public class PhotoVideoPath {

	private String path;
	private Integer itemId;
}
