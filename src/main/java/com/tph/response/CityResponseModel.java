/**
 * 
 */
package com.tph.response;

import lombok.Data;

/**
 * @author majidkhan
 *
 */
@Data
public class CityResponseModel {
	private String cityName;
	private Integer cityId;

}
