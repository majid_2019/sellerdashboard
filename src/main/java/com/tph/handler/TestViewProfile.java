/**
 * 
 */
package com.tph.handler;

import com.google.gson.Gson;
import com.tph.dao.DashboardDataDao;
import com.tph.request.SellerBaseRequestModel;
import com.tph.response.SellerBaseResponseModel;

/**
 * @author majidkhan
 *
 */
public class TestViewProfile {
	public static void main(String[] args) {
		SellerBaseResponseModel sellerBaseResponseModel = new SellerBaseResponseModel();
		//ProfileViewResponseModel sellerAddNewPostRequestModel = new ProfileViewResponseModel();
		SellerBaseRequestModel baseRequestModel = new SellerBaseRequestModel();
		
		baseRequestModel.setUserId(1);
		
		sellerBaseResponseModel = new DashboardDataDao().fetchProfileData(baseRequestModel, sellerBaseResponseModel);
		
		System.out.println("Response"+new Gson().toJson(sellerBaseResponseModel));

	}

}
