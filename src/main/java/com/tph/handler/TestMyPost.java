/**
 * 
 */
package com.tph.handler;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.tph.dao.DashboardDataDao;
import com.tph.request.SellerAddNewPostRequestModel;
import com.tph.response.PhotoVideoPath;
import com.tph.response.SellerBaseResponseModel;

/**
 * @author majidkhan
 *
 */
public class TestMyPost {
	public static void main(String[] args) {
		SellerBaseResponseModel sellerBaseResponseModel = new SellerBaseResponseModel();
		SellerAddNewPostRequestModel sellerAddNewPostRequestModel = new SellerAddNewPostRequestModel();
		PhotoVideoPath photoVideoPath = new PhotoVideoPath();
		List<PhotoVideoPath> phEs =  new ArrayList<PhotoVideoPath>(); 
		
		sellerAddNewPostRequestModel.setUserId(1);
		sellerAddNewPostRequestModel.setProductTitle("CNC Turning Machine");
		sellerAddNewPostRequestModel.setProductDetails("CNC Turning Machine, Model No. DX 200 3A, Chuck Dia 200mm, Between Centre 300mm");
		sellerAddNewPostRequestModel.setPurchaseYear("2019");
		sellerAddNewPostRequestModel.setUomId(1);
		sellerAddNewPostRequestModel.setUomAcronym("Ltr");
		sellerAddNewPostRequestModel.setBrand("LG");
		sellerAddNewPostRequestModel.setAvailableQuantity(5);
		sellerAddNewPostRequestModel.setStateId(1);
		sellerAddNewPostRequestModel.setPricePerUnit(4000.00);
		sellerAddNewPostRequestModel.setPriceType(1);
		sellerAddNewPostRequestModel.setProductLocation("Pune");
		sellerAddNewPostRequestModel.setCityId(1);
		
		photoVideoPath.setItemId(1);
		photoVideoPath.setPath("2/20190727/abc.jpg");
		phEs.add(photoVideoPath);
		sellerAddNewPostRequestModel.setPhotoVideoPathList(phEs);
		//dashboardResponseModel = 
		sellerBaseResponseModel = new DashboardDataDao().saveNewPost(sellerAddNewPostRequestModel, sellerBaseResponseModel);
		
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));

	}

}
