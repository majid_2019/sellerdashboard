/**
 * 
 */
package com.tph.response;

/**
 * @author majidkhan
 *
 */
public class UomResponseModel {
	private Integer uomId;
	private String uomName;
	private String uomAcronym;

	public Integer getUomId() {
		return uomId;
	}

	public void setUomId(Integer uomId) {
		this.uomId = uomId;
	}

	public String getUomName() {
		return uomName;
	}

	public void setUomName(String uomName) {
		this.uomName = uomName;
	}

	public String getUomAcronym() {
		return uomAcronym;
	}

	public void setUomAcronym(String uomAcronym) {
		this.uomAcronym = uomAcronym;
	}

}
