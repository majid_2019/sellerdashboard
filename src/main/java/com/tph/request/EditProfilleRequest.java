/**
 * 
 */
package com.tph.request;

import java.io.Serializable;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class EditProfilleRequest extends SellerBaseRequestModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String companyName;
	private String contactPerson;
	private Long mobileNumber;
	private String emailId;
	private String address;
	private Integer gstNumber;
	private String firstName;
	private String lastName;
}
