/**
 * 
 */
package com.tph.handler;

import java.util.Map;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import com.tph.commonmethods.CommonResponseGenerator;
import com.tph.dao.DashboardDataDao;
import com.tph.request.SellerBaseRequestModel;
import com.tph.response.SellerBaseResponseModel;

/**
 * @author majidkhan
 *
 */
public class TphHandler implements RequestHandler<Map<String, Object>, SellerBaseResponseModel> {

	private final static Gson gson = new Gson();
	private SellerBaseResponseModel sellerBaseResponseModel = new SellerBaseResponseModel();

	enum RequestType {
		sellerDashboard
	}

	@Override
	public SellerBaseResponseModel handleRequest(Map<String, Object> input, Context context) {
		try {
			String requestId = String.valueOf(input.get("requestId"));
			
			RequestType requestType = RequestType.valueOf(requestId);
			String payLoad = (String) input.get("payLoad");
			
			System.out.println("Request Type -- "+requestType);
			System.out.println("payLoad -- "+new Gson().toJson(payLoad));
			
			switch (requestType) {
			case sellerDashboard:
				SellerBaseRequestModel sellerBaseRequestModel = new SellerBaseRequestModel();
				sellerBaseRequestModel = gson.fromJson(payLoad, SellerBaseRequestModel.class);
				
				sellerBaseResponseModel = new DashboardDataDao().fetchDashboardData(sellerBaseRequestModel, sellerBaseResponseModel);
				System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));
				
				break;

			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new SellerBaseResponseModel();
			sellerBaseResponseModel = new CommonResponseGenerator().getLambdaFailResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

}
