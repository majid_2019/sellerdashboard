package com.tph.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.tph.commonmethods.CommonMoethods;
import com.tph.commonmethods.CommonResponseGenerator;
import com.tph.constants.ConstantKeys;
import com.tph.jooq.tph_db.tables.records.ProductDetailRecord;
import com.tph.request.EditProfilleRequest;
import com.tph.request.SellerAddNewPostRequestModel;
import com.tph.request.SellerBaseRequestModel;
import com.tph.request.SellerDashboardRequestModel;
import com.tph.response.ActivePostResponseModel;
import com.tph.response.IndustrialProductList;
import com.tph.response.SellerBaseResponseModel;
import com.tph.response.SellerDashboardResponseModel;
import com.tph.services.CommonInternalQueryExecuter;
import com.tph.utility.DBConnectionUtility;

/**
 * @author majidkhan
 *
 */
public class DashboardDataDao {

	private CommonInternalQueryExecuter commonInternalQueryExecuter = new CommonInternalQueryExecuter();
	private CommonResponseGenerator commonResponseGenerator = new CommonResponseGenerator();
	private CommonMoethods commonMoethods = new CommonMoethods();

	public SellerBaseResponseModel fetchDashboardData(SellerBaseRequestModel dashboardRequestModel,
			SellerBaseResponseModel sellerBaseResponseModel) {
		System.out.println("**********");

		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			System.out.println("fetchDashboardData  switch case-- " + new Gson().toJson(dashboardRequestModel));

			SellerDashboardResponseModel sellerDashboardResponseModel = new SellerDashboardResponseModel();

			sellerDashboardResponseModel.setTotalProductList(
					commonInternalQueryExecuter.fetchTotalProduct(dslContext, dashboardRequestModel.getUserId()));
			sellerDashboardResponseModel.setTotalOffers(
					commonInternalQueryExecuter.fetchTotalOffers(dslContext, dashboardRequestModel.getUserId()));
			sellerDashboardResponseModel.setTotalProductSold(
					commonInternalQueryExecuter.fetchTotalProductsold(dslContext, dashboardRequestModel.getUserId()));
			sellerDashboardResponseModel.setTotalProductView(
					commonInternalQueryExecuter.fetchTotalProductView(dslContext, dashboardRequestModel.getUserId()));
			// sellerDashboardResponseModel
			// .setCountryList(commonMoethods.getCountryList(commonInternalQueryExecuter.countryList(dslContext)));
			sellerDashboardResponseModel
					.setStateList(commonMoethods.getStateList(commonInternalQueryExecuter.stateList(dslContext)));
			sellerDashboardResponseModel
					.setUomList(commonMoethods.getUonList(commonInternalQueryExecuter.uomList(dslContext)));

			sellerDashboardResponseModel.setIndustries(commonResponseGenerator
					.IndustrialTypeRecord(commonInternalQueryExecuter.fetchIdustryList(dslContext)));

			/**
			 * Add Main Response Model to main object model.
			 */
			sellerBaseResponseModel.setMainResponse(sellerDashboardResponseModel);
			sellerBaseResponseModel = commonResponseGenerator.getSuccessMsgResponse(sellerBaseResponseModel);

			// Result<Record> result =
			// commonInternalQueryExecuter.fetchAllIndustrialData(dslContext);
			// commonResponseGenerator.fetchIndusrialList(result);
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new SellerBaseResponseModel();
			sellerBaseResponseModel = commonResponseGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * 
	 * @param dashboardRequestModel
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public SellerBaseResponseModel fetchCategoryMaster(SellerBaseRequestModel sellerBaseRequestModel,
			SellerBaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			JSONObject jsonObject = commonResponseGenerator.fetchIndCategoryList(
					commonInternalQueryExecuter.fetchCategoryMaster(dslContext, sellerBaseRequestModel));
			System.out.println("Final data -- "+jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new SellerBaseResponseModel();
			sellerBaseResponseModel = commonResponseGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return null;
	}

	/**
	 * @param dashboardRequestModel
	 * @param dashboardResponseModel
	 * @return
	 */
	public SellerBaseResponseModel fetchCountry(SellerBaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			SellerDashboardResponseModel sellerDashboardResponseModel = new SellerDashboardResponseModel();

			sellerDashboardResponseModel
					.setCountryList(commonMoethods.getCountryList(commonInternalQueryExecuter.countryList(dslContext)));

			/**
			 * Add Main Seller Response Model to main object model.
			 */
			sellerBaseResponseModel.setMainResponse(sellerDashboardResponseModel);
			sellerBaseResponseModel = commonResponseGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new SellerBaseResponseModel();
			sellerBaseResponseModel = commonResponseGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param dashboardRequestModel
	 * @param dashboardResponseModel
	 * @return
	 */
	public SellerBaseResponseModel fetchCity(SellerDashboardRequestModel dashboardRequestModel,
			SellerBaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			SellerDashboardResponseModel sellerDashboardResponseModel = new SellerDashboardResponseModel();

			sellerDashboardResponseModel.setCityList(commonMoethods
					.getCityList(commonInternalQueryExecuter.cityList(dslContext, dashboardRequestModel.getStateId())));

			/**
			 * Add Main Response Model to main object model.
			 */

			sellerBaseResponseModel.setMainResponse(sellerDashboardResponseModel);
			sellerBaseResponseModel = commonResponseGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new SellerBaseResponseModel();
			sellerBaseResponseModel = commonResponseGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param sellerAddNewPostRequestModel
	 * @param dashboardResponseModel
	 * @return
	 */
	public SellerBaseResponseModel saveNewPost(SellerAddNewPostRequestModel sellerAddNewPostRequestModel,
			SellerBaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			Integer count = commonInternalQueryExecuter.saveNewPost(dslContext, sellerAddNewPostRequestModel);
			if (count != 0) {
				sellerBaseResponseModel = commonResponseGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
			} else {
				sellerBaseResponseModel.setStatus(ConstantKeys.INTERNAL_SERVER_ERROR_CODE);
				sellerBaseResponseModel.setMessage(ConstantKeys.NOT_SAVE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new SellerBaseResponseModel();
			sellerBaseResponseModel = commonResponseGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param dashboardRequestModel
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public SellerBaseResponseModel fetchActivePost(SellerDashboardRequestModel dashboardRequestModel,
			SellerBaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			List<ActivePostResponseModel> activePostList = new ArrayList<>();
			ActivePostResponseModel activePostResponseModel;

			Result<ProductDetailRecord> record = commonInternalQueryExecuter.fetchActivePost(dslContext,
					dashboardRequestModel);
			if (record.size() > 0) {
				for (int i = 0; i < record.size(); i++) {
					activePostResponseModel = new ActivePostResponseModel();

					activePostResponseModel.setProductTitle(record.get(i).getProductName());
					activePostResponseModel.setProductDetails(record.get(i).getProductSubDesc());
					activePostResponseModel.setPricePerUnit(record.get(i).getPricePerUnit().doubleValue());
					activePostResponseModel
							.setCreateDate(commonMoethods.convertTimeStampToDate(record.get(i).getCreatedDate()));

					activePostList.add(activePostResponseModel);
				}
			}

			/**
			 * Add Active Post to main object model.
			 */
			sellerBaseResponseModel.setActivePostList(activePostList);
			sellerBaseResponseModel = commonResponseGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new SellerBaseResponseModel();
			sellerBaseResponseModel = commonResponseGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param sellerAddNewPostRequestModel
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public SellerBaseResponseModel fetchProfileData(SellerBaseRequestModel sellerBaseRequestModel,
			SellerBaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			/**
			 * Add Main Response Model to main object model.
			 */
			sellerBaseResponseModel.setProfileView(commonResponseGenerator.getProfileData(
					commonInternalQueryExecuter.fetchUserProfileData(dslContext, sellerBaseRequestModel.getUserId())));

			sellerBaseResponseModel = commonResponseGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new SellerBaseResponseModel();
			sellerBaseResponseModel = commonResponseGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	/**
	 * @param editProfilleRequest
	 * @param sellerBaseResponseModel
	 * @return
	 */
	public SellerBaseResponseModel updateProfileData(EditProfilleRequest editProfilleRequest,
			SellerBaseResponseModel sellerBaseResponseModel) {
		try (Connection connection = DBConnectionUtility.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			/**
			 * Add Main Response Model to main object model.
			 */

			if (commonInternalQueryExecuter.updateProfileData(dslContext, editProfilleRequest) != 0) {
				sellerBaseResponseModel = commonResponseGenerator.getSuccessMsgResponse(sellerBaseResponseModel);
			} else {
				sellerBaseResponseModel = commonResponseGenerator.getBasReqMsgResponse(sellerBaseResponseModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			sellerBaseResponseModel = new SellerBaseResponseModel();
			sellerBaseResponseModel = commonResponseGenerator.getExceptionResponse(sellerBaseResponseModel);
		}
		return sellerBaseResponseModel;
	}

	private List<IndustrialProductList> getTestList() {
		IndustrialProductList list = new IndustrialProductList();
		list.setProdctListPrice(22222.0);
		list.setProductListName("Inventory");
		List<IndustrialProductList> industrialProductLists = new ArrayList<IndustrialProductList>();
		industrialProductLists.add(list);
		return industrialProductLists;
	}
}
