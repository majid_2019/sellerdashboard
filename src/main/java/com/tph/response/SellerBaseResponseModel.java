/**
 * 
 */
package com.tph.response;

import java.util.List;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class SellerBaseResponseModel {

	private Integer status;
	private String message;
	private SellerDashboardResponseModel mainResponse;
	private List<ActivePostResponseModel> activePostList;
	private ProfileViewResponseModel profileView;
}
