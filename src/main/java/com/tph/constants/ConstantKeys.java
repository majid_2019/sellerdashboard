/**
 * 
 */
package com.tph.constants;

/**
 * @author majidkhan
 *
 */
public class ConstantKeys {
	
	/**
	 * Status Code
	 */
	public static Integer SUCCESS_CODE = 200;
	public static Integer NO_CONTENT_CODE = 204;
	public static Integer BAD_REQUEST_CODE = 400;
	public static Integer INTERNAL_SERVER_ERROR_CODE = 500;
	
	/**
	 * Status Message
	 */
	public static String INTERNAL_SERVER_ERROR_MSG = "Internal Server Error";
	public static String SUCCESS_MSG = "SUCCESS";
	public static String NOT_SAVE = "Data not saved";
	public static String BAD_REQUEST_MSG = "Please check request";
	public static String FAIL = "fail";
}
