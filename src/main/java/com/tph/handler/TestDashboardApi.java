/**
 * 
 */
package com.tph.handler;

import com.google.gson.Gson;
import com.tph.dao.DashboardDataDao;
import com.tph.request.SellerBaseRequestModel;
import com.tph.response.SellerBaseResponseModel;

/**
 * @author majidkhan
 *
 */
public class TestDashboardApi {

	public static void main(String[] args) {
		SellerBaseResponseModel sellerBaseResponseModel = new SellerBaseResponseModel();
		
//		SellerDashboardRequestModel dashboardRequestModel = new SellerDashboardRequestModel();
		SellerBaseRequestModel baseRequestModel = new SellerBaseRequestModel();
		
		baseRequestModel.setUserId(1);

		
		String payLoad = "{\"userId\":1}";
		System.out.println("payLoad  switch case-- "+new Gson().fromJson(payLoad, SellerBaseRequestModel.class).getUserId());
		
		
		sellerBaseResponseModel = new DashboardDataDao().fetchDashboardData(new Gson().fromJson(payLoad, SellerBaseRequestModel.class), sellerBaseResponseModel);
		System.out.println("Response - "+new Gson().toJson(sellerBaseResponseModel));
		
		
	}
}
