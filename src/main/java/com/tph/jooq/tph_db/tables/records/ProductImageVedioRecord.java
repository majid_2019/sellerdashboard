/*
 * This file is generated by jOOQ.
*/
package com.tph.jooq.tph_db.tables.records;


import java.sql.Timestamp;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record10;
import org.jooq.Row10;
import org.jooq.impl.UpdatableRecordImpl;

import com.tph.jooq.tph_db.tables.ProductImageVedio;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ProductImageVedioRecord extends UpdatableRecordImpl<ProductImageVedioRecord> implements Record10<Integer, Integer, String, String, String, Integer, Timestamp, String, Timestamp, Timestamp> {

    private static final long serialVersionUID = 2076753150;

    /**
     * Setter for <code>tph_db.product_image_vedio.id</code>.
     */
    public void setId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>tph_db.product_image_vedio.id</code>.
     */
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>tph_db.product_image_vedio.product_id</code>.
     */
    public void setProductId(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>tph_db.product_image_vedio.product_id</code>.
     */
    public Integer getProductId() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>tph_db.product_image_vedio.upload_enum</code>.
     */
    public void setUploadEnum(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>tph_db.product_image_vedio.upload_enum</code>.
     */
    public String getUploadEnum() {
        return (String) get(2);
    }

    /**
     * Setter for <code>tph_db.product_image_vedio.image_vedio_path</code>.
     */
    public void setImageVedioPath(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>tph_db.product_image_vedio.image_vedio_path</code>.
     */
    public String getImageVedioPath() {
        return (String) get(3);
    }

    /**
     * Setter for <code>tph_db.product_image_vedio.image_vedio_name</code>.
     */
    public void setImageVedioName(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>tph_db.product_image_vedio.image_vedio_name</code>.
     */
    public String getImageVedioName() {
        return (String) get(4);
    }

    /**
     * Setter for <code>tph_db.product_image_vedio.uploaded_by</code>.
     */
    public void setUploadedBy(Integer value) {
        set(5, value);
    }

    /**
     * Getter for <code>tph_db.product_image_vedio.uploaded_by</code>.
     */
    public Integer getUploadedBy() {
        return (Integer) get(5);
    }

    /**
     * Setter for <code>tph_db.product_image_vedio.uploaded_date</code>.
     */
    public void setUploadedDate(Timestamp value) {
        set(6, value);
    }

    /**
     * Getter for <code>tph_db.product_image_vedio.uploaded_date</code>.
     */
    public Timestamp getUploadedDate() {
        return (Timestamp) get(6);
    }

    /**
     * Setter for <code>tph_db.product_image_vedio.image_vedio_status</code>.
     */
    public void setImageVedioStatus(String value) {
        set(7, value);
    }

    /**
     * Getter for <code>tph_db.product_image_vedio.image_vedio_status</code>.
     */
    public String getImageVedioStatus() {
        return (String) get(7);
    }

    /**
     * Setter for <code>tph_db.product_image_vedio.updated_date</code>.
     */
    public void setUpdatedDate(Timestamp value) {
        set(8, value);
    }

    /**
     * Getter for <code>tph_db.product_image_vedio.updated_date</code>.
     */
    public Timestamp getUpdatedDate() {
        return (Timestamp) get(8);
    }

    /**
     * Setter for <code>tph_db.product_image_vedio.created_date</code>.
     */
    public void setCreatedDate(Timestamp value) {
        set(9, value);
    }

    /**
     * Getter for <code>tph_db.product_image_vedio.created_date</code>.
     */
    public Timestamp getCreatedDate() {
        return (Timestamp) get(9);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record10 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row10<Integer, Integer, String, String, String, Integer, Timestamp, String, Timestamp, Timestamp> fieldsRow() {
        return (Row10) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row10<Integer, Integer, String, String, String, Integer, Timestamp, String, Timestamp, Timestamp> valuesRow() {
        return (Row10) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return ProductImageVedio.PRODUCT_IMAGE_VEDIO.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field2() {
        return ProductImageVedio.PRODUCT_IMAGE_VEDIO.PRODUCT_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return ProductImageVedio.PRODUCT_IMAGE_VEDIO.UPLOAD_ENUM;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return ProductImageVedio.PRODUCT_IMAGE_VEDIO.IMAGE_VEDIO_PATH;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field5() {
        return ProductImageVedio.PRODUCT_IMAGE_VEDIO.IMAGE_VEDIO_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field6() {
        return ProductImageVedio.PRODUCT_IMAGE_VEDIO.UPLOADED_BY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field7() {
        return ProductImageVedio.PRODUCT_IMAGE_VEDIO.UPLOADED_DATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field8() {
        return ProductImageVedio.PRODUCT_IMAGE_VEDIO.IMAGE_VEDIO_STATUS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field9() {
        return ProductImageVedio.PRODUCT_IMAGE_VEDIO.UPDATED_DATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field10() {
        return ProductImageVedio.PRODUCT_IMAGE_VEDIO.CREATED_DATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value2() {
        return getProductId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getUploadEnum();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getImageVedioPath();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value5() {
        return getImageVedioName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value6() {
        return getUploadedBy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value7() {
        return getUploadedDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value8() {
        return getImageVedioStatus();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value9() {
        return getUpdatedDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value10() {
        return getCreatedDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductImageVedioRecord value1(Integer value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductImageVedioRecord value2(Integer value) {
        setProductId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductImageVedioRecord value3(String value) {
        setUploadEnum(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductImageVedioRecord value4(String value) {
        setImageVedioPath(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductImageVedioRecord value5(String value) {
        setImageVedioName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductImageVedioRecord value6(Integer value) {
        setUploadedBy(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductImageVedioRecord value7(Timestamp value) {
        setUploadedDate(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductImageVedioRecord value8(String value) {
        setImageVedioStatus(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductImageVedioRecord value9(Timestamp value) {
        setUpdatedDate(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductImageVedioRecord value10(Timestamp value) {
        setCreatedDate(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductImageVedioRecord values(Integer value1, Integer value2, String value3, String value4, String value5, Integer value6, Timestamp value7, String value8, Timestamp value9, Timestamp value10) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        value10(value10);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ProductImageVedioRecord
     */
    public ProductImageVedioRecord() {
        super(ProductImageVedio.PRODUCT_IMAGE_VEDIO);
    }

    /**
     * Create a detached, initialised ProductImageVedioRecord
     */
    public ProductImageVedioRecord(Integer id, Integer productId, String uploadEnum, String imageVedioPath, String imageVedioName, Integer uploadedBy, Timestamp uploadedDate, String imageVedioStatus, Timestamp updatedDate, Timestamp createdDate) {
        super(ProductImageVedio.PRODUCT_IMAGE_VEDIO);

        set(0, id);
        set(1, productId);
        set(2, uploadEnum);
        set(3, imageVedioPath);
        set(4, imageVedioName);
        set(5, uploadedBy);
        set(6, uploadedDate);
        set(7, imageVedioStatus);
        set(8, updatedDate);
        set(9, createdDate);
    }

	@Override
	public Integer component1() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer component2() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component3() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component4() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component5() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer component6() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Timestamp component7() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component8() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Timestamp component9() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Timestamp component10() {
		// TODO Auto-generated method stub
		return null;
	}
}
