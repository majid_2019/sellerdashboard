/**
 * 
 */
package com.tph.response;

/**
 * @author majidkhan
 *
 */
public class IndustrialProductList {
	private String productListName;
	private Double prodctListPrice;
	
	public String getProductListName() {
		return productListName;
	}
	public void setProductListName(String productListName) {
		this.productListName = productListName;
	}
	public Double getProdctListPrice() {
		return prodctListPrice;
	}
	public void setProdctListPrice(Double prodctListPrice) {
		this.prodctListPrice = prodctListPrice;
	}
	
}
