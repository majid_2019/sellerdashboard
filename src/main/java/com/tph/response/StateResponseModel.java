/**
 * 
 */
package com.tph.response;

/**
 * @author majidkhan
 *
 */
public class StateResponseModel {
	public String stateName;
	public Integer stateId;
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public Integer getStateId() {
		return stateId;
	}
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
}
