package com.tph.response;

import lombok.Data;

@Data
public class IndustrialSubCateData {
	private Integer id;
	private String indusSubCatName;
}
