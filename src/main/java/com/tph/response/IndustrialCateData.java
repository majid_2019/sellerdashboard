package com.tph.response;

import java.util.List;

import lombok.Data;

@Data
public class IndustrialCateData {
	private Integer id;
	private String indusCatName;
	private List<IndustrialSubCateData> subCategoryList;
}
