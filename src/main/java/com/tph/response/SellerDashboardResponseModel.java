/**
 * 
 */
package com.tph.response;

import java.util.List;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class SellerDashboardResponseModel {

	private Integer totalProductList;
	private Integer totalOffers;
	private Integer totalProductView;
	private Integer totalProductSold;
	private List<IndustrialProductList> industrialProductList;
	private List<StateResponseModel> stateList;
	private List<CityResponseModel> cityList;
	private List<CountryResponse> countryList;
	private List<UomResponseModel> uomList;
	private ProfileViewResponseModel profileResponse;
	private List<IndusDataModel> industries;
}
