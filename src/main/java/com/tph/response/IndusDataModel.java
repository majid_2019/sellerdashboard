/**
 * 
 */
package com.tph.response;

import java.util.List;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class IndusDataModel {
	private Integer id;
	private String indusName;
	private List<IndustrialCateData> industrialCateData;
}
