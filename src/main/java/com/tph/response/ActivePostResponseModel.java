/**
 * 
 */
package com.tph.response;

import java.util.List;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class ActivePostResponseModel {
	private String productTitle;
	private String productDetails;
	private String purchaseYear;
	private String brand;
	private String uomName;
	private String uomAcronym;
	private Integer availableQuantity;
	private String stateName;
	private Double pricePerUnit;
	private int priceType;
	private String productLocation;
	private String productCity;
	private String createDate;
	private List<PhotoVideoPath> photoVideoPathList;
}
