package com.tph.services;

import java.math.BigDecimal;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;

import com.google.gson.Gson;
import com.tph.jooq.tph_db.tables.CityMaster;
import com.tph.jooq.tph_db.tables.CountryMaster;
import com.tph.jooq.tph_db.tables.IndustrialType;
import com.tph.jooq.tph_db.tables.MakeAnOffer;
import com.tph.jooq.tph_db.tables.ProductDetail;
import com.tph.jooq.tph_db.tables.StateMaster;
import com.tph.jooq.tph_db.tables.UomMaster;
import com.tph.jooq.tph_db.tables.Users;
import com.tph.jooq.tph_db.tables.records.CityMasterRecord;
import com.tph.jooq.tph_db.tables.records.CountryMasterRecord;
import com.tph.jooq.tph_db.tables.records.IndustrialTypeRecord;
import com.tph.jooq.tph_db.tables.records.ProductDetailRecord;
import com.tph.jooq.tph_db.tables.records.StateMasterRecord;
import com.tph.jooq.tph_db.tables.records.UomMasterRecord;
import com.tph.request.EditProfilleRequest;
import com.tph.request.SellerAddNewPostRequestModel;
import com.tph.request.SellerBaseRequestModel;
import com.tph.request.SellerDashboardRequestModel;

public class CommonInternalQueryExecuter {

	/**
	 * @param dslContext
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public Integer fetchTotalProduct(DSLContext dslContext, Integer userId) throws Exception {
		return dslContext.selectFrom(ProductDetail.PRODUCT_DETAIL)
				.where(ProductDetail.PRODUCT_DETAIL.USER_ID.eq(userId)).fetchCount();
	}

	/**
	 * @param dslContext
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public Integer fetchTotalOffers(DSLContext dslContext, Integer userId) throws Exception {

		return dslContext.selectFrom(MakeAnOffer.MAKE_AN_OFFER)
				.where(MakeAnOffer.MAKE_AN_OFFER.SELLER_USER_ID.eq(userId)).fetchCount();
	}

	/**
	 * @param dslContext
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public Integer fetchTotalProductsold(DSLContext dslContext, Integer userId) throws Exception {

		return dslContext.selectFrom(MakeAnOffer.MAKE_AN_OFFER)
				.where(MakeAnOffer.MAKE_AN_OFFER.SELLER_USER_ID.eq(userId)).fetchCount();
	}

	/**
	 * @param dslContext
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public Integer fetchIndustrialProductList(DSLContext dslContext, Integer userId) throws Exception {

		return dslContext.selectFrom(MakeAnOffer.MAKE_AN_OFFER)
				.where(MakeAnOffer.MAKE_AN_OFFER.SELLER_USER_ID.eq(userId)).fetchCount();
	}

	/** This function is using for get all the Industrial data
	 * @param dslContext
	 * @return
	 */
	public Result<IndustrialTypeRecord> fetchIdustryList(DSLContext dslContext) {
		return dslContext.selectFrom(IndustrialType.INDUSTRIAL_TYPE)
				.where(IndustrialType.INDUSTRIAL_TYPE.IS_ACTIVE.eq("Y")).fetch();

	}

	
	/**
	 * @param dslContext
	 * @return
	 */
	public Result<Record> fetchAllIndustrialData(DSLContext dslContext) {
		String query = "select b.id as ind_id,b.name industrial_name, c.id as cat_master_id,c.cat_name industrial_cat_name , d.id as sub_cat_id,d.sub_category_name from tph_db.`industrial_cat_sub_cat_mapping` a \n" + 
				"join tph_db.industrial_type b on (a.industrial_type_id=b.id)\n" + 
				"join tph_db.categories_master c on (a.cat_id=c.id) join tph_db.sub_Categories d on (a.sub_cat_id=d.id)";

		Result<Record> record = dslContext.fetch(query);
		return record;

	}

	/**
	 * @param dslContext
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public Integer fetchTotalProductView(DSLContext dslContext, Integer userId) throws Exception {
		Integer count = 0;
		/*
		 * return dslContext.select().from(UserActivityLog.USER_ACTIVITY_LOG).join(
		 * ProductDetail.PRODUCT_DETAIL)
		 * .on(UserActivityLog.USER_ACTIVITY_LOG.USER_ID.eq(ProductDetail.PRODUCT_DETAIL
		 * .USER_ID))
		 * .where(UserActivityLog.USER_ACTIVITY_LOG.USER_ID.eq(userId)).fetchCount();
		 */
		String query = "select count(1) as total from tph_db.product_detail where user_id in(select user_id from tph_db.user_activity_log where user_id="
				+ userId + ")";

		count = Integer.valueOf(dslContext.fetch(query).getValues("total").get(0).toString());

		return count;
	}

	/**
	 * @param dslContext
	 * @return
	 * @throws Exception
	 */
	public Result<UomMasterRecord> uomList(DSLContext dslContext) throws Exception {
		return dslContext.selectFrom(UomMaster.UOM_MASTER).fetch();
	}

	/**
	 * @param dslContext
	 * @return
	 * @throws Exception
	 */
	public Result<StateMasterRecord> stateList(DSLContext dslContext) throws Exception {
		return dslContext.selectFrom(StateMaster.STATE_MASTER).fetch();
	}

	/**
	 * @param dslContext
	 * @return
	 */
	public Result<CountryMasterRecord> countryList(DSLContext dslContext) {
		return dslContext.selectFrom(CountryMaster.COUNTRY_MASTER).fetch();
	}

	/**
	 * @param dslContext
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public Result<CityMasterRecord> cityList(DSLContext dslContext, Integer stateId) throws Exception {
		return dslContext.selectFrom(CityMaster.CITY_MASTER).where(CityMaster.CITY_MASTER.STATE_ID.eq(stateId))
				.and(CityMaster.CITY_MASTER.IS_ACTIVE.eq("Y")).fetch();
	}

	/**
	 * @param dslContext
	 * @param sellerAddNewPostRequestModel
	 * @throws Exception
	 */
	public Integer saveNewPost(DSLContext dslContext, SellerAddNewPostRequestModel sellerAddNewPostRequestModel)
			throws Exception {
		ProductDetailRecord productDetailRecord = new ProductDetailRecord();

		productDetailRecord.setIndustrialTypeId(103);
		productDetailRecord.setCategoryId(103);
		productDetailRecord.setSubCategoryId(103);
		productDetailRecord.setProductName(sellerAddNewPostRequestModel.getProductTitle());
		productDetailRecord.setProductSubDesc(sellerAddNewPostRequestModel.getProductDetails());
		// productDetailRecord.setProductDescription(sellerAddNewPostRequestModel);
		productDetailRecord.setProductBrand(sellerAddNewPostRequestModel.getBrand());
		productDetailRecord.setMfgPurchaseYear(sellerAddNewPostRequestModel.getPurchaseYear());
		productDetailRecord.setQuantity(sellerAddNewPostRequestModel.getAvailableQuantity());
		productDetailRecord.setUomId(sellerAddNewPostRequestModel.getUomId());
		productDetailRecord.setPricePerUnit(new BigDecimal(sellerAddNewPostRequestModel.getPricePerUnit()));
		productDetailRecord.setPriceNegotiable((byte) sellerAddNewPostRequestModel.getPriceType());
		productDetailRecord.setProdLoc(sellerAddNewPostRequestModel.getProductLocation());
		productDetailRecord.setStateId(sellerAddNewPostRequestModel.getStateId());
		productDetailRecord.setCityId(sellerAddNewPostRequestModel.getCityId());
		productDetailRecord.setUserId(sellerAddNewPostRequestModel.getUserId());

		return dslContext.executeInsert(productDetailRecord);
	}

	/**
	 * @param dslContext
	 * @param dashboardRequestModel
	 * @return
	 */
	public Result<ProductDetailRecord> fetchActivePost(DSLContext dslContext,
			SellerDashboardRequestModel dashboardRequestModel) {
		return dslContext.selectFrom(ProductDetail.PRODUCT_DETAIL)
				.where(ProductDetail.PRODUCT_DETAIL.USER_ID.eq(dashboardRequestModel.getUserId()))
				.and(ProductDetail.PRODUCT_DETAIL.IS_ACTIVE.eq("Y")).fetch();

	}

	/**
	 * @param dslContext
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public Record fetchUserProfileData(DSLContext dslContext, Integer id) throws Exception {

		return dslContext
				.selectFrom(Users.USERS.join(CountryMaster.COUNTRY_MASTER)
						.on(Users.USERS.COUNTRY_ID.eq(CountryMaster.COUNTRY_MASTER.ID)).join(StateMaster.STATE_MASTER)
						.on(Users.USERS.STATE_ID.eq(StateMaster.STATE_MASTER.ID)).join(CityMaster.CITY_MASTER)
						.on(Users.USERS.CITY_ID.eq(CityMaster.CITY_MASTER.ID)))
				.where(Users.USERS.ID.cast(Integer.class).eq(id))
				.and(Users.USERS.ACTIVE.eq("Y").and(CityMaster.CITY_MASTER.IS_ACTIVE.eq("Y"))).fetchOne();
	}

	/**
	 * @param dslContext
	 * @param editProfilleRequest
	 * @return
	 */
	public Integer updateProfileData(DSLContext dslContext, EditProfilleRequest editProfilleRequest) {
		return dslContext.update(Users.USERS).set(Users.USERS.FIRST_NAME, editProfilleRequest.getFirstName())
				.set(Users.USERS.LAST_NAME, editProfilleRequest.getLastName())
				.set(Users.USERS.USER_MOBILE, editProfilleRequest.getMobileNumber())
				.set(Users.USERS.EMAIL, editProfilleRequest.getEmailId()).where(Users.USERS.ID.cast(Integer.class)
						.eq(editProfilleRequest.getUserId()).and(Users.USERS.ACTIVE.eq("Y")))
				.execute();
	}

	/**
	 * @param dslContext
	 * @param sellerBaseRequestModel
	 * @return 
	 * @throws Exception
	 */
	public Result<Record> fetchCategoryMaster(DSLContext dslContext, SellerBaseRequestModel sellerBaseRequestModel) throws Exception{
		String query = "select b.id as  cat_master_id, b.cat_name as cat_name, c.id as sub_category_id, c.sub_category_name as sub_category_name from tph_db.industrial_cat_sub_cat_mapping a join tph_db.categories_master b on (a.cat_id=b.id)\n" + 
				"join tph_db.sub_Categories c on(a.sub_cat_id=c.id) where a.industrial_type_id = "+sellerBaseRequestModel.getIndustrialTypeId();
		Result<Record> record = dslContext.fetch(query);
		
		return record;
	}
}
