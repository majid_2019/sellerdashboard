/**
 * 
 */
package com.tph.request;

import java.util.List;

import com.tph.response.PhotoVideoPath;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class SellerAddNewPostRequestModel extends SellerBaseRequestModel {
	private String productTitle;
	private String productDetails;
	private String purchaseYear;
	private String brand;
	private String uomName;
	private String uomAcronym;
	private Integer availableQuantity;
	private String stateName;
	private Double pricePerUnit;
	private int priceType;
	private String productLocation;
	private String productCity;
	private List<PhotoVideoPath> photoVideoPathList;
}
