/**
 * 
 */
package com.tph.response;

import lombok.Data;

/**
 * @author majidkhan
 *
 */
@Data
public class CountryResponse {
	private Integer countryId;
	private String countryName;
}
